//
//  PhotosCellViewInput.swift
//  Test
//
//  Created by Vladimir Vasilyev on 26.11.2017.
//  Copyright © 2017 Vladimir Vasilyev. All rights reserved.
//

import UIKit

protocol PhotosCellViewInput {
    
    func updateImage(with image: UIImage)
    func updateTitle(text: String)
    func updateInfo(action: @escaping () -> ())
    
}
