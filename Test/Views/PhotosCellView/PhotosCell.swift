//
//  PhotosCell.swift
//  Test
//
//  Created by Vladimir Vasilyev on 26.11.2017.
//  Copyright © 2017 Vladimir Vasilyev. All rights reserved.
//

import UIKit

class PhotosCell: UICollectionViewCell, PhotosCellViewInput {

    @IBOutlet weak var photoImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    private var infoActionClosure: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func infoButtonAction(_ sender: UIButton) {
        infoActionClosure?()
    }
    
    func updateImage(with image: UIImage) {
        photoImageView.image = image
    }
    
    func updateTitle(text: String) {
        titleLabel.text = text
    }
    
    func updateInfo(action: @escaping () -> ()) {
        infoActionClosure = action
    }
}
