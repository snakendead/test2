//
//  IntroIntroViewInput.swift
//  Test
//
//  Created by Vladimir Vasilyev on 24/11/2017.
//  Copyright © 2017 Revolage. All rights reserved.
//

import Foundation

@objc protocol IntroViewInput: class {
    
    func showPhotos()
    func photosButtonChangeTitle()
    func alertText(hide: Bool)
}
