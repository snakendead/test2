//
//  IntroIntroViewController.swift
//  Test
//
//  Created by Vladimir Vasilyev on 24/11/2017.
//  Copyright © 2017 Revolage. All rights reserved.
//

import UIKit

class IntroViewController: BaseModuleViewController, IntroViewInput {

    var output: IntroViewOutput!

    @IBOutlet weak var showPhotosButton: UIButton!
    @IBOutlet weak var warningLabel: UILabel!
    
    //MARK: Initialization

    override func setupDependencies() {

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupActions()
        output.viewIsReady()
    }

    private func setupUI() {

    }

    private func setupActions() {

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output.viewWillAppear()
    }
    
    //MARK:- Actions

    @IBAction func showPhotosButtonAction(_ sender: UIButton) {
        output.showPhotosDidTapped()
    }

    //MARK: IntroViewInput methods

    func showPhotos() {
        performSegue(withIdentifier: "IntroToPhotos", sender: nil)
    }
    
    func photosButtonChangeTitle() {
        showPhotosButton.setTitle("Retry", for: .normal)
    }
    
    func alertText(hide: Bool) {
        warningLabel.isHidden = hide
    }
}
