//
//  IntroIntroAssembly.swift
//  Test
//
//  Created by Vladimir Vasilyev on 24/11/2017.
//  Copyright © 2017 Revolage. All rights reserved.
//

import UIKit
import Typhoon

class IntroAssembly: TyphoonAssembly {

    @objc public dynamic func IntroViewControllerAssembly() -> AnyObject {
        return TyphoonDefinition.withClass(IntroViewController.self, configuration: {
            (definition) in
            definition?.injectProperty(Selector(("output")), with: self.IntroPresenterAssembly())
        }) as AnyObject
        //Perform an initializer injection
    }

    @objc public dynamic func IntroPresenterAssembly() -> AnyObject {
        return TyphoonDefinition.withClass(IntroPresenter.self, configuration: {
            (definition) in
            definition?.injectProperty(Selector(("viewInput")), with: self.IntroViewControllerAssembly())
            definition?.injectProperty(Selector(("interactor")), with: self.IntroInteractorAssembly())

            definition?.perform(afterAllInjections: Selector(("setupDependencies")))
        }) as AnyObject
    }

    @objc public dynamic func IntroInteractorAssembly() -> AnyObject {
        return TyphoonDefinition.withClass(IntroInteractor.self, configuration: {
            (definition) in
            definition?.useInitializer(Selector(("init:")), parameters: {
                (initializer) in
                initializer?.injectParameter(with: ServiceFacade.shared)
            })
            definition?.injectProperty(Selector(("output")), with: self.IntroPresenterAssembly())
        }) as AnyObject
    }

}
