//
//  IntroIntroPresenter.swift
//  Test
//
//  Created by Vladimir Vasilyev on 24/11/2017.
//  Copyright © 2017 Revolage. All rights reserved.
//

import Foundation
import Photos

class IntroPresenter: NSObject, IntroViewOutput, IntroInteractorOutput, IntroModuleInput {

    weak var viewInput: IntroViewInput!
    weak var moduleOutput: IntroModuleOutput?

    var interactor: IntroInteractorInput!

    override dynamic init() {
        super.init()
        initialize()
    }

    func initialize() {

    }

    func setupDependencies() {

    }

    //MARK: IntroInteractorOutput methods
    
    func didObtainStatus(_ status: PHAuthorizationStatus) {
        
        switch status {
        case .authorized:
            viewInput.alertText(hide: true)
            viewInput.showPhotos()
        case .denied:
            fallthrough
        case .restricted:
            viewInput.alertText(hide: false)
            viewInput.photosButtonChangeTitle()
        case .notDetermined:
            viewInput.alertText(hide: true)
            interactor.requestPhotosPermissions()
        }
    }
    
    //MARK: IntroViewOutput methods

    internal func viewIsReady() {

    }

    internal func viewWillAppear() {

    }

    internal func showPhotosDidTapped() {
        interactor.getPhotosPermissionsStatus()
    }
    
    //MARK:

    internal func getViewLayer() -> Any {
        return viewInput
    }
}
