//
//  IntroIntroInteractorOutput.swift
//  Test
//
//  Created by Vladimir Vasilyev on 24/11/2017.
//  Copyright © 2017 Revolage. All rights reserved.
//

import Foundation
import Photos

@objc protocol IntroInteractorOutput: class {

    func didObtainStatus(_ status: PHAuthorizationStatus)
}
