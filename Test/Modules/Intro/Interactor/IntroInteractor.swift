//
//  IntroIntroInteractor.swift
//  Test
//
//  Created by Vladimir Vasilyev on 24/11/2017.
//  Copyright © 2017 Revolage. All rights reserved.
//

import Foundation
import Photos

class IntroInteractor: NSObject, IntroInteractorInput {

    weak var output: IntroInteractorOutput!

    var serviceFacade: ServiceFacade

    dynamic init(_ serviceFacade: ServiceFacade) {
        self.serviceFacade = serviceFacade
        super.init()
    }
    
    func getPhotosPermissionsStatus() {
        let status = PHPhotoLibrary.authorizationStatus()
        output.didObtainStatus(status)
    }
    
    func requestPhotosPermissions() {
        PHPhotoLibrary.requestAuthorization({
            status in
            DispatchQueue.main.async {
                self.output.didObtainStatus(status)
            }
        })
    }
}
