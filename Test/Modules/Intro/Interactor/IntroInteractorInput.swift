//
//  IntroIntroInteractorInput.swift
//  Test
//
//  Created by Vladimir Vasilyev on 24/11/2017.
//  Copyright © 2017 Revolage. All rights reserved.
//

import Foundation

@objc protocol IntroInteractorInput: class {
    func getPhotosPermissionsStatus()
    func requestPhotosPermissions()
}
