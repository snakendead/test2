//
//  PhotosPhotosCollectionMediator.swift
//  Test
//
//  Created by Vladimir Vasilyev on 18/11/2017.
//  Copyright © 2017 Revolage. All rights reserved.
//

import UIKit

class PhotosCollectionMediator: NSObject, UICollectionViewDelegate, UICollectionViewDataSource {

    var dataCount: Int = 0 {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    var fillCellData: ((_ cell: PhotosCellViewInput, _ indexPath: IndexPath) -> Void)?
    var cellSelectionClosure: ((_ indexPath: IndexPath) -> Void)?
    var loadDataCallback: (() -> Void)?

    private var lastLoadedOffset: CGFloat = 0
    
    dynamic init(_ xibName: String) {
        self.xibName = xibName
        super.init()
    }

    private var xibName: String
    private var collectionView: UICollectionView!

    func fillCollection(_ collectionView: UICollectionView) {
        self.collectionView = collectionView
        setupCollectionUI()
    }

    func setupCollectionUI() {
        let nib = UINib(nibName: xibName, bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: xibName)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: xibName, for: indexPath)
        fillCellData?(cell as! PhotosCellViewInput, indexPath)
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = (scrollView.contentSize.height - scrollView.frame.size.height)
        if scrollView.contentOffset.y == offset {
            lastLoadedOffset = offset
            loadDataCallback?()
        }
    }
}
