//
//  PhotosPhotosAssembly.swift
//  Test
//
//  Created by Vladimir Vasilyev on 18/11/2017.
//  Copyright © 2017 Revolage. All rights reserved.
//

import UIKit
import Typhoon

class PhotosAssembly: TyphoonAssembly {

    @objc public dynamic func PhotosViewControllerAssembly() -> AnyObject {
        return TyphoonDefinition.withClass(PhotosViewController.self, configuration: {
            (definition) in
            definition?.injectProperty(Selector(("output")), with: self.PhotosPresenterAssembly())
            definition?.injectProperty(Selector(("collectionMediator")), with: self.PhotosCollectionMediatorAssembly())
        }) as AnyObject
        //Perform an initializer injection
    }

    @objc public dynamic func PhotosPresenterAssembly() -> AnyObject {
        return TyphoonDefinition.withClass(PhotosPresenter.self, configuration: {
            (definition) in
            definition?.injectProperty(Selector(("viewInput")), with: self.PhotosViewControllerAssembly())
            definition?.injectProperty(Selector(("interactor")), with: self.PhotosInteractorAssembly())
            definition?.injectProperty(Selector(("collectionMediator")), with: self.PhotosCollectionMediatorAssembly())

            definition?.perform(afterAllInjections: Selector(("setupDependencies")))
        }) as AnyObject

    }

    @objc public dynamic func PhotosCollectionMediatorAssembly() -> AnyObject {
        return TyphoonDefinition.withClass(PhotosCollectionMediator.self, configuration: {
            (definition) in
            definition?.useInitializer(Selector(("init:")), parameters: {
                (initializer) in
                //TODO:
                initializer?.injectParameter(with: "PhotosCell")
            })
        }) as AnyObject
    }

    @objc public dynamic func PhotosInteractorAssembly() -> AnyObject {
        return TyphoonDefinition.withClass(PhotosInteractor.self, configuration: {
            (definition) in
            definition?.useInitializer(Selector(("init:")), parameters: {
                (initializer) in
                initializer?.injectParameter(with: ServiceFacade.shared)
            })
            definition?.injectProperty(Selector(("output")), with: self.PhotosPresenterAssembly())
        }) as AnyObject
    }

}
