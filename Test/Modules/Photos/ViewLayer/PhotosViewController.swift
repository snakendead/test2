//
//  PhotosPhotosViewController.swift
//  Test
//
//  Created by Vladimir Vasilyev on 18/11/2017.
//  Copyright © 2017 Revolage. All rights reserved.
//

import UIKit

class PhotosViewController: BaseModuleViewController, PhotosViewInput {

    var output: PhotosViewOutput!
    weak var collectionMediator: PhotosCollectionMediator!

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var progressView: UIProgressView!
    
    //MARK: Initialization

    override func setupDependencies() {
        collectionMediator.fillCollection(collectionView)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupActions()
        output.viewIsReady()
    }

    private func setupUI() {

    }

    private func setupActions() {

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output.viewWillAppear()
    }

    //MARK: PhotosViewInput methods
    func updateProgress(_ progress: Float) {
        progressView.progress = progress
    }
    
    func showHashAlert(_ md5: String, size: String) {
        let message = """
        Hash - \(md5)
        Size - \(size)
        """
        let alert = UIAlertController(title: "MD5 Hash of selected photo", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let actionThanks = UIAlertAction(title: "Thanks!", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(actionThanks)
        present(alert, animated: true, completion: nil)
    }
}
