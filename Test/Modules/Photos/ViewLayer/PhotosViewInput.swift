//
//  PhotosPhotosViewInput.swift
//  Test
//
//  Created by Vladimir Vasilyev on 18/11/2017.
//  Copyright © 2017 Revolage. All rights reserved.
//

import Foundation

@objc protocol PhotosViewInput: BaseViewInput {

    func showHashAlert(_ md5: String, size: String)
    func updateProgress(_ progress: Float)
}
