//
//  PhotosPhotosInteractor.swift
//  Test
//
//  Created by Vladimir Vasilyev on 18/11/2017.
//  Copyright © 2017 Revolage. All rights reserved.
//

import Foundation

class PhotosInteractor: NSObject, PhotosInteractorInput {

    weak var output: PhotosInteractorOutput!

    var serviceFacade: ServiceFacade

    private var page: Int = 0
    
    dynamic init(_ serviceFacade: ServiceFacade) {
        self.serviceFacade = serviceFacade
        super.init()
    }
    
    func fetchPhotos() {
        DispatchQueue.global().async {
            [weak self] in
            self?.serviceFacade.photoManager.fetchPhoto(page: self?.page ?? 0,
                                                        callback: {
                                                            [weak self]
                                                            (photos, error) in
                                                            self?.reactFetching(photos, error: error)
                }, progress: {
                    [weak self]
                    (progress) in
                    self?.reactProgress(progress)
                }, preloadedCallback: { (preload, errror) in
                    self?.reactPreload(preload)
            })
            
            self?.page += 1
        }
    }
    
    private func reactFetching(_ photos: [PhotoItem], error: String?) {
        DispatchQueue.main.async {
            [unowned self] in
            if let message = error {
                self.output?.photosFetchingFailure(message)
                return
            }
            
            self.output?.photosFetchingSuccess(photos)
        }
    }
    
    private func reactProgress(_ value: Float) {
        DispatchQueue.main.async {
            [unowned self] in
            self.output?.photosFetchingProgress(value)
        }
    }

    private func reactPreload(_ photos: [PhotoItem]) {
        DispatchQueue.main.async {
            [unowned self] in
            self.output?.photosFetchingPreload(photos)
        }
    }

    deinit {
        serviceFacade.photoManager.stopPhotoFetching()
    }
}
