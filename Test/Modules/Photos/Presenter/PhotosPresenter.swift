//
//  PhotosPhotosPresenter.swift
//  Test
//
//  Created by Vladimir Vasilyev on 18/11/2017.
//  Copyright © 2017 Revolage. All rights reserved.
//

import Foundation

class PhotosPresenter: NSObject, PhotosViewOutput, PhotosInteractorOutput, PhotosModuleInput {

    weak var viewInput: PhotosViewInput!
    weak var moduleOutput: PhotosModuleOutput?

    var interactor: PhotosInteractorInput!
    var collectionMediator: PhotosCollectionMediator!

    private var collectionData = [PhotoItem]()

    override dynamic init() {
        super.init()
        initialize()
    }

    func initialize() {

    }

    func setupDependencies() {
        collectionMediator.fillCellData = {
            [weak self]
            (cellInput, indexPath) in
            guard let self_ = self else { return }
            let data = self_.collectionData[indexPath.row]
            cellInput.updateTitle(text: data.name)
            cellInput.updateImage(with: data.image)
            cellInput.updateInfo {
                self_.showAlertHash(md5: data.md5, size: data.dataSize)
            }
        }

        collectionMediator.cellSelectionClosure = {
            [weak self]
            (indexPath) in
            guard let self_ = self else { return }
            let data = self_.collectionData[indexPath.row]
            self_.showAlertHash(md5: data.md5, size: data.dataSize)
        }

        collectionMediator.loadDataCallback = {
            [weak self] in
            guard let self_ = self else { return }
            self_.interactor.fetchPhotos()
        }

        collectionMediator.dataCount = 0
    }

    //MARK: PhotosViewOutput methods
    
    internal func viewIsReady() {
        
    }
    
    internal func viewWillAppear() {
        interactor.fetchPhotos()
    }
    
    //MARK: PhotosInteractorOutput methods

    func photosFetchingFailure(_ errorMessage: String) {
        viewInput.showError(errorMessage)
    }
    
    func photosFetchingSuccess(_ data: [PhotoItem]) {
        collectionData.append(contentsOf: data)
        collectionMediator.dataCount = collectionData.count
    }

    func photosFetchingProgress(_ value: Float) {
        viewInput.updateProgress(value)
    }

    func photosFetchingPreload(_ data: [PhotoItem]) {
        collectionData.insert(contentsOf: data, at: 0)
        collectionMediator.dataCount = collectionData.count
    }

    //MARK:

    internal func getViewLayer() -> Any {
        return viewInput
    }
    
    //MARK: Private
    
    private func showAlertHash(md5: String, size: String) {
        viewInput.showHashAlert(md5, size: size)
    }
}
