//
//  PhotosPhotosViewOutput.swift
//  Test
//
//  Created by Vladimir Vasilyev on 18/11/2017.
//  Copyright © 2017 Revolage. All rights reserved.
//

import Foundation

@objc protocol PhotosViewOutput: class {

    func viewIsReady()
    func viewWillAppear()

}
