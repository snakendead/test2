//
//  PhotosPhotosInteractorOutput.swift
//  Test
//
//  Created by Vladimir Vasilyev on 18/11/2017.
//  Copyright © 2017 Revolage. All rights reserved.
//

import Foundation

@objc protocol PhotosInteractorOutput: class {

    func photosFetchingFailure(_ errorMessage: String)
    func photosFetchingSuccess(_ data: [PhotoItem])
    func photosFetchingProgress(_ value: Float)
    func photosFetchingPreload(_ data: [PhotoItem])
}
