//
//  ServiceFacade.swift
//  Test
//
//  Created by Vladimir Vasilyev on 24.11.2017.
//  Copyright © 2017 Vladimir Vasilyev. All rights reserved.
//

import UIKit

class ServiceFacade: NSObject {
    
    static let shared = ServiceFacade()
    
    let photoManager = PhotoManager()
    
}
