//
//  PhotoManager.swift
//  Test
//
//  Created by Vladimir Vasilyev on 26.11.2017.
//  Copyright © 2017 Vladimir Vasilyev. All rights reserved.
//

import UIKit
import Photos

class PhotoManager: NSObject {

    typealias PhotosCallback = (_ photos: [PhotoItem], _ error: String?) ->Void
    typealias ProgressCallback = (_ progressValue: Float) -> Void
    
    private var callbackFinish: PhotosCallback?
    private var callbackProgress: ProgressCallback?
    private var callbackPreloaded: PhotosCallback?

    private var cachedFetchResults: PHFetchResult<PHAsset>? {
        didSet {
            if let cached = cachedFetchResults {
                cacheFirstDate = cached.firstObject?.creationDate ?? Date(timeIntervalSince1970: 0)
            }
        }
    }
    private var cacheFirstDate: Date = Date(timeIntervalSince1970: 0)
    
    private var requestIds = [PHImageRequestID]()
    
    private let pageStep = 50
    
    func stopPhotoFetching() {
        for id in requestIds {
            PHImageManager.default().cancelImageRequest(id)
        }
        
        requestIds = []
    }
    
    func fetchPhoto(page: Int,
                    callback: @escaping PhotosCallback,
                    progress: @escaping ProgressCallback,
                    preloadedCallback: @escaping PhotosCallback) {
        
        if page == 0 {
            cachedFetchResults = nil
            callbackPreloaded = preloadedCallback
            subscribeOnPreload()
        }
        
        callbackFinish = callback
        callbackProgress = progress
        
        var errorMessage: String? = nil
        
        defer {
            if let message = errorMessage {
                self.callbackFinish?([], message)
            }
            else {
                requestResults(page: page)
            }
        }
        
        guard cachedFetchResults == nil else {
            return
        }

        let fetchResults = self.allPhotosFetchResult()
        
        guard fetchResults.count > 0 else {
            errorMessage = "You have 0 photos"
            return
        }
        
        cachedFetchResults = fetchResults
        
    }
    
    func requestResults(page: Int) {
        
        guard let fetchResults = cachedFetchResults else {
            self.callbackFinish?([], "Caching photos error")
            return
        }
        
        
        var result = [PhotoItem]()
        
        let group = DispatchGroup()
        
        let start = page * pageStep
        let nextStep = (start + pageStep)
        let count = fetchResults.count
        let last = count < nextStep ? count : nextStep
        
        let currentCount = last - start
        var progressValue: Float = 0.0
        let stepPercent = 100 / Float(currentCount)
        let step = stepPercent / 100

        guard last > start else {
            self.callbackFinish?([], nil)
            return
        }
        
        for i in start..<last {
            let asset = fetchResults.object(at: i)
            group.enter()
            
            let id = requestPhotoItem(asset: asset, callback: {
                [weak self]
                (item) in
                if let item_ = item { result.append(item_) }
                
                progressValue += step
                self?.callbackProgress?(progressValue)
                group.leave()
            })

            requestIds.append(id)
        }
        
        group.notify(queue: DispatchQueue.main) {
            [weak self] in
            self?.callbackFinish?(result, nil)
        }
    }
    
    private func photoItem(from data: Data, info: [AnyHashable : Any]?) -> PhotoItem {
        let name = photoName(from: info)
        
        let item = PhotoItem(with: name, data: data)
        return item
    }

    private func photoItem(from image: UIImage, info: [AnyHashable : Any]?, filename: String?) -> PhotoItem {
        let name = filename != nil ? filename! : photoName(from: info)
        
        let item = PhotoItem(with: name, image: image)
        return item
    }
    
    private func photoName(from info: [AnyHashable : Any]?) -> String {
        var name = "unknown"
        if let fileName = (info?["PHImageFileURLKey"] as? NSURL)?.lastPathComponent {
            name = fileName
        }
        
        return name
    }
    
    private func subscribeOnPreload() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.applicationForegroundReact),
                                               name: NSNotification.Name.UIApplicationWillEnterForeground,
                                               object: nil)
    }
    
    @objc private func applicationForegroundReact() {
        let newResult = allPhotosFetchResult()
        
        guard let newFirstDate = newResult.firstObject?.creationDate else {
            return
        }
        
        guard newFirstDate > cacheFirstDate else {
            // No new photos
            return
        }
        
        let group = DispatchGroup()
        var preloadResult = [PhotoItem]()
        
        for i in 0..<newResult.count {
            let asset = newResult.object(at: i)
            
            guard let date = asset.creationDate else { continue }
            
            // It's old photos
            if date <= cacheFirstDate { break }
            
            group.enter()
            _ = requestPhotoItem(asset: asset, callback: {
                (item) in
                guard let item_ = item else { return }
                preloadResult.append(item_)
                group.leave()
            })
        }
        
        cacheFirstDate = newFirstDate

        group.notify(queue: DispatchQueue.main) {
            [weak self] in
            self?.callbackPreloaded?(preloadResult, nil)
        }
    }
    
    private func requestPhotoItem(asset: PHAsset, callback: @escaping (_ item: PhotoItem?) -> Void) -> PHImageRequestID {
        
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = false
        requestOptions.deliveryMode = .fastFormat
        requestOptions.isNetworkAccessAllowed = true
        
        let manager = PHImageManager.default()
        
        let resources = PHAssetResource.assetResources(for: asset)
        let filename = resources.first?.originalFilename

        let id = manager.requestImage(for: asset,
                                      targetSize: CGSize(width: 500, height: 500),
                                      contentMode: .aspectFill,
                                      options: requestOptions,
                                      resultHandler: {
                                        [weak self]
                                        (image, info) in
                                        if let image_ = image {
                                            if let photoItem = self?.photoItem(from: image_, info: info, filename: filename) {
                                                callback(photoItem)
                                                return
                                            }
                                        }

                                        callback(nil)
        })
        
        return id
    }
    
    private func allPhotosFetchResult() -> PHFetchResult<PHAsset> {
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        fetchOptions.includeHiddenAssets = false
        fetchOptions.includeAllBurstAssets = false
        fetchOptions.includeAssetSourceTypes = .typeUserLibrary
        
        let fetchResults = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        
        return fetchResults
    }
}

