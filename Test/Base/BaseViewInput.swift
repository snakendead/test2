//
//  BaseViewInput.swift
//  Test
//
//  Created by Vladimir Vasilyev on 24.11.2017.
//  Copyright © 2017 Vladimir Vasilyev. All rights reserved.
//

import UIKit

@objc protocol BaseViewInput: class {
    func showError(_ message: String)
}
