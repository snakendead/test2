//
//  BaseModuleViewController.swift
//  Test
//
//  Created by Vladimir Vasilyev on 24.11.2017.
//  Copyright © 2017 Vladimir Vasilyev. All rights reserved.
//

import UIKit

class BaseModuleViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupDependencies()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupDependencies() {
        
    }
    
    @objc func showError(_ message: String) {
        let alert = UIAlertController(title: "Error!", message: message, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okayAction)

        present(alert, animated: true, completion: nil)
    }

}
