//
//  PhotoItem.swift
//  Test
//
//  Created by Vladimir Vasilyev on 26.11.2017.
//  Copyright © 2017 Vladimir Vasilyev. All rights reserved.
//

import UIKit

class PhotoItem: NSObject {

    var name: String
    var md5: String {
        return data.md5
    }
    var data: Data
    
    var dataSize: String {
        let countFormatter = ByteCountFormatter()
        countFormatter.allowedUnits = [.useKB, .useBytes]
        countFormatter.countStyle = .file
        let string = countFormatter.string(fromByteCount: Int64(data.count))
        return string
    }
    
    private var imageCached: UIImage?
    var image: UIImage {
        if let image = imageCached {
            return image
        }
        if let image = UIImage(data: data) {
            imageCached = image
            return image
        }
        return UIImage()
    }
    
    init(with name: String,
         data: Data) {
        self.name = name
        self.data = data
        super.init()
    }
    
    init(with name: String,
         image: UIImage) {
        self.name = name
        self.imageCached = image
        self.data = UIImagePNGRepresentation(image) ?? Data()
        super.init()
    }
    
}

extension Data {
    var md5: String {
        var digest = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
        _ =  self.withUnsafeBytes { bytes in
            CC_MD5(bytes, CC_LONG(self.count), &digest)
        }
        var digestHex = ""
        for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }
        return digestHex
    }
}
